import { Module, forwardRef } from '@nestjs/common';
import { AppController } from './app.controller';
import { GlobalModule } from './common/global.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './common/guards/auth.guard';

@Module({
  imports: [
    GlobalModule,  
    // TypeOrmModule.forRootAsync({
    //   name: 'user-db',
    //   inject: [ConfigService],
    //   useFactory: (config: ConfigService) => config.get('userDatabase')
    // }),
    forwardRef(() => UserModule),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
