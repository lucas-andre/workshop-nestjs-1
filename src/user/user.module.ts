import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Role } from './entities/role.entity';
import { UsersController } from './controller/users.controller';
import { RolesController } from './controller/roles.controller';
import { UsersService } from './service/users.service';
import { RolesService } from './service/roles.service';

@Module({
    imports: [],
    // imports: [TypeOrmModule.forFeature([User, Role], 'user-db')],
    controllers: [UsersController, RolesController],
    providers: [UsersService, RolesService],
    exports: [UsersService, RolesService],
})
export class UserModule {}
