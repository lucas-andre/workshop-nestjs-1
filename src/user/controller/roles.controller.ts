import { Controller, Get, NotFoundException, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiParam, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Role } from '../entities/role.entity';
import { RolesService } from '../service/roles.service';

@ApiTags('roles')
@Controller('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @ApiOperation({ summary: 'Find Roles' })
  @ApiResponse({ status: 200 })
  @Get()
  async findAll(@Query() query: any): Promise<Role[]> {
    // return this.rolesService.findAll(query);
    return;
  }

  @ApiOperation({ summary: 'Get Role By Id' })
  @ApiParam({ name: 'id', required: true })
  @ApiResponse({ status: 200, description: 'The role exists.' })
  @ApiResponse({ status: 404, description: 'Role does not exist.' })
  @Get(':id')
  async findOne(@Param('id', new ParseIntPipe()) id: number): Promise<Role> {
    // const role = await this.rolesService.findOne(id);
    // if (!role) {
    //   throw new NotFoundException();
    // }

    // return role;
    return; 
  }
}
