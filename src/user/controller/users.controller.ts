import { Body, Controller, Get, NotFoundException, Param, Post, Req, Query } from '@nestjs/common';
import { ApiParam, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from '../dtos/createUser.dto';
import { User } from '../entities/user.entity';
import { UsersService } from '../service/users.service';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiOperation({ summary: 'Create User'})
  @ApiResponse({ status: 201, description: 'The user has been successfully created.' })
  @ApiResponse({ status: 400, description: 'Role does not exists.' })
  @ApiResponse({ status: 409, description: 'User already exists.' })
  @Post()
  async create(@Body() userDto: CreateUserDto) {
    // const user = await this.usersService.create(userDto);
    // return user;
    return;
  }

  @ApiOperation({ summary: 'Find Users' })
  @ApiResponse({ status: 200 })
  @Get()
  async findAll(@Query() query: any): Promise<{ data: User[]; count: number }> {
    // return this.usersService.findAll(query);
    return;
  }

  @ApiOperation({ summary: 'Get User By Id' })
  @ApiParam({ name: 'id', required: true })
  @ApiResponse({ status: 200, description: 'The user exists.' })
  @ApiResponse({ status: 404, description: 'User does not exist.' })
  @Get(':id')
  async findOne(@Param('id') id: string, @Req() req) {
    // const user = await this.usersService.findOne({ id });
    // if (!user) {
    //   throw new NotFoundException();
    // }

    // return user;
    return;
  }
}
