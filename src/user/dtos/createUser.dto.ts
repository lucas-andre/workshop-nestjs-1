import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({
    example: 'Henry',
    description: 'User First Name',
  })
  @IsString()
  readonly username: string;

  @ApiProperty({
    example: 'ivan.mirson@cencosud.cl',
    description: 'User Email',
  })
  @IsString()
  readonly email: string;

  @ApiProperty({
    example: 'Password1',
    description: 'User Password',
  })
  @IsString()
  readonly password: string;

  @ApiProperty({
    example: 'admin',
    description: 'User Role',
  })
  @IsString()
  readonly role: string;

}
