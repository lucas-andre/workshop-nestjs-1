import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { compare, hash } from 'bcryptjs';
import { ConfigService } from '@nestjs/config';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../dtos/createUser.dto';
import { User } from '../entities/user.entity';
import { RolesService } from './roles.service';

@Injectable()
export class UsersService {
  constructor(
    // @InjectRepository(User)
    // private readonly userRepository: Repository<User>,
    private readonly rolesService: RolesService,
    private readonly configService: ConfigService,
  ) {}

  // async create(user: CreateUserDto): Promise<User> {
  //   const exists = await this.userRepository.count({
  //     email: user.email,
  //   });

  //   if (exists) {
  //     throw new ConflictException('User already exists');
  //   }

  //   const role = await this.rolesService.findOne({ name: user.role });

  //   if (!role) {
  //     throw new BadRequestException('Role does not exists');
  //   }

  //   let newUser = new User();
  //   newUser.username = user.username;
  //   newUser.email = user.email;
  //   newUser.role = role;
  //   newUser.password = await this.hashPassword(user.password);

  //   const exists2 = await this.userRepository.count({
  //     email: user.email,
  //   });

  //   if (exists2) {
  //     throw new ConflictException('User already exists');
  //   }

  //   const userEntity = await this.userRepository.save(newUser);

  //   return userEntity;
  // }


  // async updatePassword(email: string, password: string): Promise<User> {
  //   const user = await this.findOne({
  //     email,
  //   });

  //   if (!user) {
  //     throw new ConflictException('Invalid user email');
  //   }

  //   user.password = await this.hashPassword(password);

  //   return await this.userRepository.save(user);
  // }

  // async hashPassword(password: string): Promise<string> {
  //   return await hash(password, this.configService.get('auth.saltLength'));
  // }

  // async validatePassword(candidatePassword: string, hash: string): Promise<boolean> {
  //   return await compare(candidatePassword, hash);
  // }

  // async findAll(query: any): Promise<{ data: any[]; count: number }> {
  //   const { limit, offset, ...rest } = query;
  //   return null;
  // }

  // async findOne(query: any): Promise<User | undefined> {
  //   return null
  // }

  // async edit(id: number, user: CreateUserDto): Promise<User> {
  //   return null
  // }

  // async remove(id: number) {
  //   return null;
  // }
}
