import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { Role } from './role.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid') id: string;
  @Column() username: string;
  @Column({ nullable: true }) first_name: string;
  @Column() last_name: string;
  @Column() email: string;
  @Column() password: string;
  @ManyToOne(type => Role, { cascade: false, nullable: false }) role: Role;
  @CreateDateColumn() created_at: Date;
  @UpdateDateColumn() updated_at: Date;
}
