import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as timeout from 'connect-timeout';
import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: true,
      preflightContinue: false,
    }
  });

  // CONFIGURACIONES DE LA API
  const configService: ConfigService = app.get(ConfigService);
  console.log('configService', configService);

  // VALIDACIONES DE REQUEST(Data Transfer Objects)
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    })
  )
  // SWAGGER
  const swaggerOptions = new DocumentBuilder()
    .setTitle('Workshop NestJS')
    .setDescription('Basic API REST for manage Users')
    .setVersion('0.0.1')
    .build()
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api', app, swaggerDocument);
  
  // HELMET
  app.use(helmet());
  // TIMEOUT DE REQUEST
  app.use(timeout('120s'));
  
  // OBTENER PUERTO DEL ENTERONO (ENVIROMENT)
  const port = configService.get('app.port', 3000); 
  console.log('port', port);
  // LA APLICACION COMIENZA A ESCUCHAR EN EL PUERTO CONFIGURADO
  await app.listen(port);
}
bootstrap();
