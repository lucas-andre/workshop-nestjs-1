import { Module, Global } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { JwtModule } from '@nestjs/jwt';
import appConfig from '../config/app';
// import userDatabaseConfig from '../config/userDatabase';
import authConfig from '../config/auth';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig,
        //  userDatabaseConfig,
          authConfig]
    }),
    JwtModule.registerAsync({
      imports: [ConfigService],
      useFactory: async (config: ConfigService) => ({
        secretOrPrivateKey: config.get('auth.jwtSecretKey'),
        signOptions: {
          expiresIn: config.get('auth.jwtExpiration'),
        }
      }),
      inject: [ConfigService]
    })
  ],
  providers: [],
  controllers: [],
  exports: [JwtModule, ConfigModule]
})
export class GlobalModule { }