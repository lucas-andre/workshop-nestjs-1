export interface JwtPermission {
    c: string;
    a: string;
  }
  
  export interface JwtPayload {
    id: string;
    email: string;
    role: string;
  }
  