import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../user/entities/user.entity';
import { Role } from '../user/entities/role.entity';

@Module({
    imports: [TypeOrmModule.forFeature([User, Role], 'user-db')],
    controllers: [],
    providers: [],
    exports: []
})
export class AuthModule {}
