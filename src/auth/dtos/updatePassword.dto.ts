import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class UpdatePasswordDto {
  @ApiProperty({
    example: 'ivan.mirson@cencosud.cl',
    description: 'User Email',
  })
  @IsString()
  readonly email: string;

  @ApiProperty({
    example: 'Password1',
    description: 'Current Password',
  })
  @IsString()
  readonly current_password: string;

  @ApiProperty({
    example: 'Password2',
    description: 'New Password',
  })
  @IsString()
  readonly new_password: string;

  @ApiProperty({
    example: 'Password2',
    description: 'Repeat New Password',
  })
  @IsString()
  readonly new_password_2: string;
}
