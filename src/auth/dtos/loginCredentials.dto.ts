import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginCredentials {
  @ApiProperty({
    example: 'ivan.mirson@cencosud.cl',
    description: 'User Email',
  })
  @IsString()
  readonly email: string;

  @ApiProperty({
    example: 'Password1',
    description: 'User Password',
  })
  @IsString()
  readonly password: string;
}
