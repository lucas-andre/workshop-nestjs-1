import { ConflictException, Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { JwtPayload } from '../../common/interfaces/jwt-payload.interface';
import { User } from '../../user/entities/user.entity';
import { UsersService } from '../../user/service/users.service';
import { LoginCredentials } from '../dtos/loginCredentials.dto';
import { UpdatePasswordDto } from '../dtos/updatePassword.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UsersService,
    private readonly configService: ConfigService,
  ) {}

  // async login(credentials: LoginCredentials): Promise<any> {
  //   const user = await this.userService.findOne({
  //     email: credentials.email,
  //   });

  //   if (!user) {
  //     throw new ConflictException('Invalid credentials');
  //   }
  //   const loginValid = await this.userService.validatePassword(credentials.password, user.password);

  //   if (!loginValid) {
  //     throw new ConflictException('Invalid credentials');
  //   }
  //   return await this.generateUserJwt(user);
  // }

  // async updatePassword(updatePasswordDto: UpdatePasswordDto): Promise<User> {
  //   if (updatePasswordDto.new_password !== updatePasswordDto.new_password_2) {
  //     throw new BadRequestException('New passwords must be the same');
  //   }

  //   if (updatePasswordDto.current_password === updatePasswordDto.new_password) {
  //     throw new BadRequestException('New passwords must be different from current password');
  //   }

  //   const user = await this.userService.findOne({
  //     email: updatePasswordDto.email,
  //   });

  //   if (!user) {
  //     throw new ConflictException('Invalid credentials');
  //   }
  //   const loginValid = await this.userService.validatePassword(updatePasswordDto.current_password, user.password);

  //   if (!loginValid) {
  //     throw new ConflictException('Invalid credentials');
  //   }

  //   const updatedUser = await this.userService.updatePassword(updatePasswordDto.email, updatePasswordDto.new_password);

  //   return await this.generateUserJwt(updatedUser);
  // }

  // async generateUserJwt(user: User): Promise<any> {
  //   const jwtPayload: JwtPayload = {
  //     id: user.id,
  //     email: user.email,
  //     role: user.role.name,
  //   //   permissions: user.role.permissions.map(p => ({
  //   //     c: p.controller,
  //   //     a: p.action,
  //   //   })),
  //   };

  //   const accessToken = this.jwtService.sign(jwtPayload, {
  //     issuer: this.configService.get('auth.jwtIssuer'),
  //     expiresIn: this.configService.get('auth.jwtExpiration'),
  //   });

  //   return {
  //     expiresIn: this.configService.get('auth.jwtExpiration'),
  //     jwtPayload,
  //     accessToken,
  //   };
  // }

  // async refreshToken(payload: JwtPayload): Promise<any> {
  //   const accessToken = this.jwtService.sign(payload, {
  //     issuer: this.configService.get('auth.jwtIssuer'),
  //     expiresIn: this.configService.get('auth.jwtExpiration'),
  //   });

  //   return {
  //     expiresIn: this.configService.get('auth.jwtExpiration'),
  //     accessToken,
  //   };
  // }
}
